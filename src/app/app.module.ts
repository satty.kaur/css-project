import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FixedPositioningComponent } from './fixed-positioning/fixed-positioning.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import { StaticPositioningComponent } from './static-positioning/static-positioning.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RelativePositioningComponent } from './relative-positioning/relative-positioning.component';
import { AbsoluteComponent } from './absolute/absolute.component';
import { FloatPositionComponent } from './float-position/float-position.component';
import { DisplayComponent } from './display/display.component';
import { AlignmentComponent } from './alignment/alignment.component';
import { FlexboxComponent } from './flexbox/flexbox.component';

@NgModule({
  declarations: [
    AppComponent,
    FixedPositioningComponent,
    StaticPositioningComponent,
    HomeComponent,
    PageNotFoundComponent,
    RelativePositioningComponent,
    AbsoluteComponent,
    FloatPositionComponent,
    DisplayComponent,
    AlignmentComponent,
    FlexboxComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path: 'home', component: HomeComponent},
      {path: 'position-fixed', component: FixedPositioningComponent},
      {path: 'position-static', component: StaticPositioningComponent},
      {path: 'position-relative', component: RelativePositioningComponent},
      {path: 'position-absolute', component: AbsoluteComponent},
      {path: 'position-float', component: FloatPositionComponent},
      {path: 'position-display', component: DisplayComponent},
      {path: 'position-align', component: AlignmentComponent},
      {path: 'position-flextbox', component: FlexboxComponent},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: '**', component: PageNotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
